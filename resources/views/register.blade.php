<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SIM Perpus XYZ | Register</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/sign-in/">

    <link href="{{ asset("/css/bootstrap.min.css") }}" rel="stylesheet">
    <link href="{{ asset("/css/signin.css") }}" rel="stylesheet">

</head>
<body class="text-center">
    <div class="container" style="display: flex; justify-content: center; align-items: center;">
        <div class="card col-sm-5">
            <main class="form-signin">
                <form>
                <img class="mb-4 user-avatar rounded-circle border" src='{{ asset("/images/logo.png") }}' alt="" width="100" height="100">
                <h1 class="h3 mb-3 fw-normal">Create Account</h1>
            
                <div class="form-floating">
                    <input type="email" class="form-control" id="floatingInput" placeholder="name@example.com">
                    <label for="floatingInput">Email address</label>
                </div>
                <div class="form-floating">
                    <input type="password" class="form-control" id="floatingPassword" placeholder="Password">
                    <label for="floatingPassword">Password</label>
                </div>
                <div class="form-floating">
                    <input type="password" class="form-control" id="floatingPassword" placeholder="Password">
                    <label for="floatingPassword">Confirm Password</label>
                </div>
        
                
                <button class="w-100 btn btn-lg btn-primary" type="submit">Create</button>
                <p>Already have account? <a href="{{ url("/") }}">Login here</a></p>
                </form>
            </main>
        </div>
    </div>
        
        
            
</body>
</html>